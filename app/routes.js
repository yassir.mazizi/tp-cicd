const user = require('./users');

module.exports = function(app) {
  // GET request on /
  app.get('/', (req, res) => {
    const language = process.env.LANGUAGE || 'en';
    if(language === 'en') {
      res.status(200).json('You successfully deployed the webserver!');
    } else if(language === 'fr') {
      res.status(200).json('Tu as bien déployé le serveur web !');
    } else if(language === 'ru') {
      res.status(200).json('Вы успешно развернули веб-сервер!');
    } else {
      res.status(400).json(`${language} is not recognized as a language code`);
    }
  });

  // GET request on /ok
  app.get('/ok', (req, res) => {
    res.status(200).json('The webserver is correctly running');
  });

  // GET request on /error
  app.get('/error', (req, res) => {
    res.status(418).json('I\'m a teapot');
  });

  // POST request on /user
  app.post('/user', (req, res) => {
    if(!req.body.login) {
      res.status(403).json('Body request is invalid')
    } else if(user.isMember(req.body.login)) {
      res.status(200).json(`${req.body.login} is indeed a member`);
    } else {
      res.status(400).json(`${req.body.login} is not a member`);
    }
  });

  // POST request on /admin
  app.post('/admin', (req, res) => {
    if (!(req.body.login && req.body.password)) {
      res.status(403).json('Body request is invalid')
    }
    const login = req.body.login;
    const password = req.body.password;
    const adminStatus = user.adminStatus(login, password);
    res.status(adminStatus.status).json(adminStatus.msg);
  });
};
