// Import basic modules
var http = require('http');
var express = require('express');
require('dotenv').config();

// Create express app
var app = express();

// Configure request format JSON
app.use(express.urlencoded({extended: true}));
app.use(express.json());

// Configure app
app.set('port', process.env.PORT || 3000);
require('./app/routes')(app);

// Start app
http.createServer(app).listen(app.get('port'), function() {
	console.log(`Webserver is starting on port ${app.get('port')}...`);
});

module.exports = app;
